classdef AsiakkuudenKehittaminen < handle
    %ASIAKKUUDENKEHITTAMINEN K�sittelee asiakkaan kehitt�misen dataa
    %   Luokka, jolla tutkitaan asiakkaan sitoutumista asiakkuuden
    %   kehitt�misen vaiheessa Facebookin postauksien, kommenttien ja
    %   tykk�yksien avulla.
	%	Sis�lt�� datan esik�sittelyn, muunnokset ja visualisoinnin.
    %   Tiedonlouhinnan hy�dynt�minen asiakkaan sitoutumisen tutkimisessa -
    %   Pro gradu -tutkielma
    %   4.6.2019 Merja Halonen 
    
	
    properties
        tablePost;
        tableComment;
        tableLike;
        tableMember
        data;
        likes;
        paivat;
        nimet;
        lopData;
        dataPoisRiveja;
        lopId;
        skaalattuData;
        logData;
        logSkaalattuData;
        logSkaalattuDataMiinus;
        skaalattudataPoisRiveja;
        logDataPoisRiveja;
        logSkaalattuDataPois;
        logSkaalattuDataPoisMiinus;
        figMuuttujat;
		
        %asiakkuuden s�ilytt�miseen data
        sailData; 
    end
    
	
    methods
        function obj = AsiakkuudenKehittaminen(filePost, fileLike,...
                fileMember, fileComment)
            %% ASIAKKUUDENKEHITTAMINEN Konstruktori asiakkuudenkehitt�minen
            % Konstruktori asiakkuudenkehitt�misen datalle
            %   Lataa asiakkaan kehitt�misen tiedoston ja luo siit�
            %   tabletin. Muuttaa my�s sarakkeita oikeaan muotoon
            %   filePost = postauksien tiedosto
            %   fileLike = tykk�yksien tiedosto
            %   fileMember = j�senten tiedosto
            %   fileCOmment = kommenttien tiedosto
            
            obj.tablePost = readtable(filePost);
            obj.tableLike = readtable(fileLike);
            obj.tableMember = readtable(fileMember, 'Delimiter',',');
            obj.tableComment = readtable(fileComment);
            
            % tablePost sarakkeiden muokkaaminen
            %obj.tablePost.likes = str2double(obj.tablePost.likes);
            obj.tablePost.likes(isnan(obj.tablePost.likes))=0;
            
            % tableComment sarakkeiden muokkaaminen
            obj.tableComment.rid(isnan(obj.tableComment.rid))=0;
                    
            % tableLike sarakkeiden muokkaamista
            obj.tableLike.cid = str2double(obj.tableLike.cid);
            obj.tableLike.cid(isnan(obj.tableLike.cid))=0;
   
        end
        
        
        function kayttajatMatriisinRiveille(obj, sarLkm)
            %% KAYTTAJATMATRIISINRIVEILLE Muodostaa matriisin k�ytt�jist�
            % Muodostaa matriisin kaikkista k�ytt�jist�, jotka postanneet, 
            % kommentoineet ja tyk�nneet
            %   Otetaan jokainen yksitt�inen k�ytt�j� kaikista paitsi
            %   Member-taulukosta ja muodostetaan data-matriisi, jossa
            %   k�ytt�j�t riveitt�in ja sarLkm mukaan sarakkeita.
            %   sarLkm = sarakkeiden lkm
            
            [~,postA,~] = unique(obj.tablePost.id);
            post = obj.tablePost(postA,2:3);
            
            [~,commentA,~] = unique(obj.tableComment.id);
            comment = obj.tableComment(commentA,4:5);
            
            [~,likeA,~] = unique(obj.tableLike.id);
            like = obj.tableLike(likeA,4:5);
            
            V = unique([post; comment; like]);
            obj.data = zeros(height(V),sarLkm);
            Vsort = sortrows(V,1);
            
            obj.nimet = Vsort(:,2);
            [values, index, ids] = unique(obj.nimet, 'rows', 'stable');
            
            obj.data(:,(end-1)) = table2array(Vsort(:,1));
            obj.lopId = obj.data(index,(end-1));
            
            obj.data(:,end) = ids;
       
            %Alustetaan paivat-matriisi, johon lasketaan k�ytt�j�n
            %postauksien ja kommenttien eri p�iv�t
            obj.paivat = zeros(height(V),4);
            obj.paivat(:,1) = table2array(Vsort(:,1));
            obj.paivat(:,end) = ids;
            
        end
        
        
        function likesRakenne(obj, sar)
            %% LIKESRAKENNE Muodostaa likes-rakenteen
            %   Muodostaa likes-rakenteen, jossa on k�ytt�jien eri
            %   postauksien tykk�ykset (PL) (angry, haha jne.) ja
            %   kommenttien tykk�ykset (KL)
            %   sar = sarakkeiden lkm, jotka luodaan
            
            obj.likes.PL = zeros(length(obj.data),sar);
            obj.likes.KL = zeros(length(obj.data),sar);
            obj.likes.PLlop = 1;
            obj.likes.KLlop = 1;
            
        end
   
   
        function tyhjennaData(obj, D)
            %% TYHJENNADATA Tyhjennet��n data, jottei vie muistia
            %   D = tyhjennett�v� data
            
            obj = setfield(obj, D,  []);
            
        end
        
        
        function laskePost(obj, D, post, like, share)
            %% LASKEPOSTAUKSET Laskee postauksien lkm
            %   Laskee postauksien lkm k�ytt�jitt�in D-tablesta. Lis�ksi
            %   laskee, kuinka monta tykk�yst� (like) ja jakoa (share)
            %   k�ytt�j�n tekem�ll� postauksella on. Laskee my�s, kuinka
            %   monena eri p�iv�n� k�ytt�j� on postannut ja asettaa tiedon
            %   paivat-matriisiin.
            %   D = taulukko, josta tiedot lasketaan, tablePost
            %   post = sarake, johon postauksien lkm laitetaan data-mat
            %   like = sarake, johon postauksien tykk�yksien lkm laitetaan
            %       data-matriisissa
            %   share = sarake, johon postauksien jakojen lkm laitetaan
            %       data-matriisissa
            
            dataNimi = getfield(obj, D);
            
            for j=1:length(obj.data)
                rows = dataNimi.id == obj.data(j,(end-1));
                I = find(rows == 1);
                obj.data(j,post) = length(unique(dataNimi.pid(I)));
                obj.data(j,share) = sum(unique(dataNimi.shares(I)));
                obj.data(j,like) = sum(unique(dataNimi.likes(I)));
                
                %Laskee kuinka monena eri p�iv�n� k�ytt�j� on postannut
                T = dataNimi(I,4); % 4 on timeStamp sarake
                obj.paivat(j,2) = length(unique(dateshift(T{:,1},...
                    'start','day')));
            end
            
        end
        
        
        function laskeLike(obj, D, sarMat)
            %% LASKELIKE Laskee tykk�yksien lkm
            %   Laskee tykk�yksien lkm k�ytt�jitt�in D-tablesta.
            %   D = taulukko, josta tiedot otetaan, tableLike
            %   sarMat = sarake, johon tieto laitetaan data-matriisissa
            
            dataNimi = getfield(obj, D);
            
            for j=1:length(obj.data)
                
                %postauksen tykk�ykset
                rowsPL = dataNimi.id == obj.data(j,(end-1)) &...
                    dataNimi.cid == 0;
                %kommenttien tykk�ykset
                rowsKL = dataNimi.id == obj.data(j,(end-1)) &...
                    dataNimi.cid > 0;
                
                IPL = find(rowsPL == 1); %postauksien tykk�ykset
                IKL = find(rowsKL == 1); %kommenttien tykk�ykset
                
                obj.data(j,sarMat) = length(IPL);
                obj.data(j,(sarMat+1)) = length(IKL);
                
                % Laskee k�ytt�jitt�in, kuinka monta erilaista
                % tykk�ysmuotoa k�ytt�j� on tehnyt postauksiin (PL) ja
                % kommentteihin (KL).
                
                [PL,ipla,iplc] = unique(dataNimi.response(IPL));
                [KL,ikla,iklc] = unique(dataNimi.response(IKL));
                
                PLcounts = accumarray(iplc,1);
                valuePLcounts = [PL, num2cell(PLcounts)];
                
                obj.likes.PL(j,(end-1)) = obj.data(j,(end-1));
                obj.likes.PL(j,end) = obj.data(j,end);
                
                
                if(~isempty(valuePLcounts))
                    
                    [mPL,nPL] = size(valuePLcounts);
                    for k=1:mPL
                        switch valuePLcounts{k,1}
                            case 'ANGRY'
                                obj.likes.PL(j,1) = valuePLcounts{k,2};
                            case 'HAHA'
                                obj.likes.PL(j,2) = valuePLcounts{k,2};
                            case 'LIKE'
                                obj.likes.PL(j,3) = valuePLcounts{k,2};
                            case 'LIKES'
                                obj.likes.PL(j,4) = valuePLcounts{k,2};
                            case 'LOVE'
                                obj.likes.PL(j,5) = valuePLcounts{k,2};
                            case 'SAD'
                                obj.likes.PL(j,6) = valuePLcounts{k,2};
                            case 'WOW'
                                obj.likes.PL(j,7) = valuePLcounts{k,2};
                            otherwise
                                warning('Not repsonse')
                        end
                    end
                end
                
                KLcounts = accumarray(iklc,1);
                valueKLcounts = [KL, KLcounts];
                
                obj.likes.KL(j,(end-1)) = obj.data(j,(end-1));
                obj.likes.KL(j,end) = obj.data(j,end);
                
                
                if(~isempty(valueKLcounts))
                    
                    [mKL,nKL] = size(valueKLcounts);
                    for l=1:mKL
                        switch valueKLcounts{l,1}
                            case 'ANGRY'
                                obj.likes.KL(j,1) = valueKLcounts{l,2};
                            case 'HAHA'
                                obj.likes.KL(j,2) = valueKLcounts{l,2};
                            case 'LIKE'
                                obj.likes.KL(j,3) = valueKLcounts{l,2};
                            case 'LIKES'
                                obj.likes.KL(j,4) = valueKLcounts{l,2};
                            case 'LOVE'
                                obj.likes.KL(j,5) = valueKLcounts{l,2};
                            case 'SAD'
                                obj.likes.KL(j,6) = valueKLcounts{l,2};
                            case 'WOW'
                                obj.likes.KL(j,7) = valueKLcounts{l,2};
                            otherwise
                                warning('Not repsonse')
                        end
                    end
                end
            end
            
        end
        
        
        function laskeComment(obj, D, sarCid, sarRid)
            %% LASKECOMMENT Laskee kommenttien lkm
            %   Laskee k�ytt�j�n tekemien omien kommenttien m��r�n (K) ja
            %   laskee k�ytt�j�n kommenttiin tehdyn kommentin m��r�n (KK)
            %   Lis�ksi laskee kuinka monena eri p�iv�n� k�ytt�j� on
            %   kommentoinut.
            %   D = Taulukko, josta tieto otetaan, tableComment
            %   sarCid = sarake, johon kommentin m��r� laitetaan data-mat.
            %   sarRid = sarake, johon kommentin kommentti m��r� laitetaan
            %            data-matriisissa
            
            dataNimi = getfield(obj, D);
            
            for j=1:length(obj.data)
                
                %k�ytt�j�n oma komentti, eka komentti
                rowsK = dataNimi.id == obj.data(j,(end-1)) &...
                    dataNimi.rid == 0;
                %kommentti kommenttiin
                rowsKK = dataNimi.id == obj.data(j,(end-1)) &...
                    dataNimi.rid > 0;
                
                IK = find(rowsK == 1);
                IKK = find(rowsKK == 1);
                
                obj.data(j,sarCid) = length(IK);
                obj.data(j,sarRid) = length(IKK);
                
                % Laskee kuinka monena eri p�iv�n� k�ytt�j� on kommentoinut
                T = dataNimi(IK,3);
                obj.paivat(j,3) = length(unique(dateshift(T{:,1},...
                    'start','day')));
            end
            
        end
        
        
        function laskePostuksienComment(obj, lisaa, D, T)
            %% LASKEPOSTAUKSIENCOMMENT Laskee postauksien kommentit
            %   Laskee k�ytt�j�n postauksiin tehtyjen kommenttien m��r�n ja
            %   n�ihin kommentteihin tehtyjen kommenttien m��r�n. Laskee
            %   tiedot uuteen matriisiin, jossa lopDatan tiedot.
            %   lisaa = montako saraketta lis�� uuteen sailData matriisiin
            %   D = tablePost
            %   T = tableComment
            
            [m,n] = size(obj.lopData);
            
            obj.sailData = zeros(m,n+lisaa);
            obj.sailData(:,1:(n-1)) = obj.lopData(:,1:(end-1));
            obj.sailData(:,end) = obj.lopData(:,end);
            
            dataNimi = getfield(obj, D);
            dataTaulu = getfield(obj,T);
            
            for j=1:length(obj.sailData)
                rowsPost = dataNimi.id == obj.sailData(j,end);
                Ipost = find(rowsPost == 1);
                pid = unique(dataNimi.pid(Ipost)); %postauksien id:t
                rowsComment = ismember(dataTaulu.pid,pid);
                Icomment = find(rowsComment);
                taulu = dataTaulu(Icomment,:);
                rowsK = taulu.id ~= obj.sailData(j,end) & taulu.rid == 0; 
                rowsKK = taulu.id ~= obj.sailData(j,end) & taulu.rid > 0;
                
                IK = find(rowsK);
                IKK = find(rowsKK);

                obj.sailData(j,(n)) = length(IK);
                obj.sailData(j,(end-1)) = length(IKK);
            end
            
        end
        
       
        function poistaNollarivit(obj)
            %% POISTANOLLARIVIT Poistaa mahdolliset nollarivit
            %   Poistaa rivit, joissa k�ytt�j�ll� ei ole mit��n tapahtumia
            %   eli tykk�yksi�, postauksia ja kommentteja.
            
            I = find(all(obj.data(:,1:10) == 0,2));
            obj.data(I,:) = [];
            %             obj.data(~any(obj.data(:,1:(end-1)),2), : ) = [];
        end
        
        
        function paivatDataan(obj, sarakkeet)
            %% PAIVATDATAAN Paivat-matriisista paivat data-matriisiin
            %   Ottaa paivat-matriisista eri p�iv�t data-matriisiin.
            %   sarakkeet = sarakkeet, joihin tieto data-matriisissa
            %   laitetaan
            
            for j=1:length(obj.data)
                I = find(obj.paivat(:,1) == obj.data(j,(end-1)));
                obj.data(j,sarakkeet) = obj.paivat(I,2:(end-1));
            end
        end
        
        
        function ryhmanJasen(obj, T, M)
            %% RYHMAJASEN Tarkistaa, onko j�senen� ryhm�ss�
            %   Merkitsee data-matriisiin, l�ytyyk� j�sen member-taulukosta
            %   T = taulukko, josta tarkistetaan, tableMember
            %   M = matriisi, johon tieto laitetaan, data
            
            dataTable = getfield(obj, T);
            dataMat = getfield(obj, M);
            
            for j=1:length(dataMat)
                obj.lopData(j,10) = length(find((dataTable.id ==...
                    obj.lopData(j,end)) == 1));
            end
            
        end
        
        
        function yhdistaaTuplaKayttajat(obj)
            %% YHDISTAATUPLAKAYTTAJAT Yhdist�� k�ytt�j�t, jotka tuplasti
            %   Yhdist�� k�ytt�jien, joiden nimi enemm�n kuin yhdesti,
            %   rivit. Jos k�ytt�j� vaihtanut profiilikuvaa, niin antaa
            %   uuden id:n, mutta nimi s�ilyy.
            
            obj.lopData = splitapply(@(rows) sum(rows, 1),...
                obj.data(:,1:(end-1)), obj.data(:,end));
            obj.lopData(:,end) = obj.lopId;
            
        end
        
        
        function yhdistaaTuplaKayttajatLikes(obj)
            %% YHDISTAATUPLAKAYTTAJAT Yhdist�� k�ytt�j�t, jotka tuplasti
            %   Yhdist�� k�ytt�jien, joiden nimi enemm�n kuin yhdesti,
            %   rivit. Jos k�ytt�j� vaihtanut profiilikuvaa, niin antaa
            %   uuden id:n, mutta nimi s�ilyy.
            
            obj.likes.PLlop = splitapply(@(rows) sum(rows, 1),...
                obj.likes.PL(:,1:(end-1)), obj.likes.PL(:,end));
            obj.likes.PLlop(:,end) = obj.lopId;
            %obj.likes.PLlop(:,1) = [];
            
            obj.likes.KLlop = splitapply(@(rows) sum(rows, 1),...
                obj.likes.KL(:,1:(end-1)), obj.likes.KL(:,end));
            obj.likes.KLlop(:,end) = obj.lopId;
            %obj.likes.KLlop(:,1) = [];

        end
        
        
        function poistaaTietytKayttajat(obj, D, vertaus, sar, luku)
            %% POISTAATIETYTKAYTTAJAT Antaa data, jossa tietyt k�ytt�j�t
            %   Poistaa tietyt k�ytt�j�t, jotka on yli tai ali tietyn luvun
            %   D = data, josta poistetaan
            %   vertaus = tehd��nk� poistossa vertaus yli/ali
            %   sar = sarake, jota verrataan
            %   luku = johon verrataan
            
            obj.dataPoisRiveja = getfield(obj, D);
            
            if ( strcmp('yli',vertaus) )
                obj.dataPoisRiveja...
                    (obj.dataPoisRiveja(:,sar) > luku, :) = [];
            else
                obj.dataPoisRiveja...
                    (obj.dataPoisRiveja(:,sar) <= luku, :) = [];
            end
        end
        
        
        function histogrammiMuuttujat(obj, D, x_Labels)
            %% HISTOGRAMMIMUUTTUJAT Muodostaa histogrammin muuttujista
            %   Muodostaa histogrammin, josta n�kee muuttujien jakauman
            %   D = data, josta kuvaaja muodostetaan
            %   x_Labels = muuttujat, joista histogrammi muodostetaan
            
            dataNimi = getfield(obj, D);
            nimi = 'AsiakkuudenKehittaminen';
            obj.figMuuttujat = figure('Name',nimi);
            n = length(x_Labels);
            
            k = 0;
            
            for i=1:n
                k = k + 1;
                if (i == 1)
                    subplot('position', [0.06 0.11 .18 .79]),...
                        histogram(dataNimi(:,k))
                end
                if (i == 2)
                    subplot('position', [0.30 0.11 .18 .79]),...
                        histogram(dataNimi(:,k))
                end
                if (i == 3)
                    subplot('position', [0.54 0.11 .18 .79]),...
                        histogram(dataNimi(:,k))
                end
                if (i == 4)
                    subplot('position', [0.78 0.11 .18 .79]),...
                        histogram(dataNimi(:,k))
                end
                
                ax = gca;
                ax.FontSize = 10;
                tx = xlabel(x_Labels{i});
                ty = ylabel('K�ytt�jien lkm')
                tx.FontSize  =  12;
                ty.FontSize  = 12;
            end
            
            text( -5, 126000, 'Muuttujien jakaumat', 'FontSize', 14',...
                'FontWeight', 'Bold', 'HorizontalAlignment', 'right',...
                'VerticalAlignment', 'bottom' ) ;
        end
        
        
        function minMaxSkaalaus(obj, D, A, sar)
            %% MINMAXSKAALAUS Tekee datalle min-max-skaalauksen
            %   Skaalaa datan v�lille [-1,1] tai [0,1] tietult� v�lilt�
            %   D = data, jolle skaalaus tehd��n
            %   A = data, johon skaalaus sijoitetaan
            %   sar = viimeinen sarake, joka huomioidaan
            
            dataNimi = getfield(obj, D);
            [m,n] = size(dataNimi);
            skaalattu = zeros(m,n);
            obj = setfield(obj, A, zeros(m,n));
			
			sarMin = min(dataNimi(:,1:sar));
			sarMax = max(dataNimi(:,1:sar));
            
            skaalattu(:,1:sar) = rescale(dataNimi(:,1:sar),'InputMin',...
                sarMin,'InputMax',sarMax);
            skaalattu(:,end) = dataNimi(:,end);
                
            obj = setfield(obj, A, skaalattu);
        end
        
        
        function logaritmiMuutos(obj, D, A, logvika)
            %% LOGARITMIMUUTOS Tekee datalle logartimimuunnoksen
            %   Muodostetaan data, jossa tehty tietyille sarakkeille
            %   logaritmimuunnos.
            %   D = data, jolle muunnos tehd��n
            %   A = data, johon muutos tallennetaan
            %   logvika = sarake, mihin asti muutos tehd��n
            
            dataNimi = getfield(obj, D);
            [m,n] = size(dataNimi);
            logaritmi = zeros(m,n);
            obj = setfield(obj, A, zeros(m,n));
            
            dataNimi(dataNimi(:,1:logvika) == 0) = 0.01;
            logaritmi(:,1:logvika) = log(dataNimi(:,1:logvika));
            logaritmi(:,end) = dataNimi(:,end);
            obj = setfield(obj, A, logaritmi);
            
        end
        
        
        function kneePointKuvaaja(obj, indeksit, K, nimet, vali)
            %% KNEEPOINTKUVAAHA KneePoint -kuvaaja validointi-indekseist�
            %   Muodostaa plot-kuvaajat klustereittain indekseist� 
            %   jokaiselle klusterien m��r�ll�.
            %   indeksit = rakenne, joissa validointi-indeksit ovat
            %   klustereittain
            %   K = klusterien m��r�
            %   nimet = klusterointien nimet
            %   vali = muuttujien sarakkeet, joista tehty klusterointi
            
            fields = fieldnames(indeksit);
            
            [m,n] = size(indeksit);
            l = length(fields);
            
            for j=1:n 
                figure('Name',nimet(j).nimi);
                
                p = 0;
                for k=2:3:l
                    p = p + 1;
                    D = indeksit(j).(fields{k});
                    
                    x = 2:K;
                    y = D(2:end,1);
                    subplot(2,4,p)
                    plot(x,y)
                    xlim([2 K])

                    axis 'auto y'
                    xticks(2:vali:K);
                    xticklabels({2:vali:K});
                    
                    title(fields(k));
                    xlabel('K');
                    ylabel('Indeksin arvo');
                    
                    x_pos = indeksit(j).(fields{k+2});
                    y_pos = indeksit(j).(fields{k+1});
                    hold on
                    plot(x_pos,y_pos,'r*');
                end
            end
        end
        
        
        function valitutKuvaajat(obj, indeksit, K, nimet, vali, v, i, lkm)
            %% KNEEPOINTKUVAAHA KneePoint -kuvaaja validointi-indekseist�
            %   Muodostaa plot-kuvaajat klustereittain indekseist� 
            %   jokaiselle klusterien m��r�ll�.
            %   indeksit = rakenne, joissa validointi-indeksit ovat
            %   klustereittain
            %   K = klusterien m��r�
            %   nimet = klusterointien nimet
            %   vali = kuvaajan x-akselin v�li
            %   v = indeksit (sarakkeet), joista kuvaaja muodostetaan
            %   i = toisen for loopin p��t�sindeksi
            %   lkm = kuvaajien lkm
            
            fields = fieldnames(indeksit);
            
            [m,n] = size(indeksit);
            l = length(fields);
            
            figure('Name',nimet(1).nimi);

            for j=i:i
                
                p = 0;
                for k=v
                    p = p + 1;
                    D = indeksit(j).(fields{k});
                    
                    x = 2:K;
                    y = D(2:end,1);
                    subplot(1,lkm,p)
                    plot(x,y,'LineWidth',1.0)
                    xlim([2 K])
                    axis 'auto y'
                    xticks(2:vali:K);
                    xticklabels({2:vali:K});
                    
                    ax = gca;
                    ax.FontSize = 10;
                    tit = title(fields(k));
                    tit.FontSize = 13;
                    tx = xlabel('K (klustereiden lkm)');
                    ty = ylabel('Indeksin arvo');
                    tx.FontSize  =  12;
                    ty.FontSize  = 12;
                    
                    x_pos = indeksit(j).(fields{k+2});
                    y_pos = indeksit(j).(fields{k+1});
                    hold on
                    plot(x_pos,y_pos,'.', 'MarkerSize',15);
                end
            end
        end
        
        
        function [clust, dataMdPost, dataMdComment, dataMdLikePost,...
                dataMdLikeComment] = klusterinTarkastelu (obj, D, K,...
                kohta, kayttajaID, klustData)
            %% KLUSTERINTARKASTELU Klusterin analysointiin tunnuslukuja
            %   Laskee klustereista mediaaneja ja prosentteja analysointiin
            %   D = data, jossa alkuperaiset tiedot k�ytt�jist�
            %   K = klusterien m��r�
            %   kohta = kohta, mist� klusteroidusta datasta otetaan tiedot
            %   		laskentaan
            %   kayttajatID = datan sarake, jossa on k�ytt�jien ID:t
            %   klustData = klusteroitu data
			
            %   clust = palauttaa clust-rakenteen, jossa lasketut
            %   tunnusluvut klustereittain
            %   dataMdPost = D-datan postauksien mediaani
            %   dataMdComment = D-datan kommenttien mediaani
            %   dataMdLikeComment = D-datan Like mediaani
            
            dataNimi = getfield(obj, D);
            [m,n] = size(dataNimi);
            dataMdPost = median(dataNimi(:,1));
            dataMdComment = median(dataNimi(:,2));
            dataMdLikePost = median(dataNimi(:,3));
            dataMdLikeComment = median(dataNimi(:,4));
                        
            for j=1:K+1
                clust(j).inds = j;
                clust(j).tiedot = j;
                clust(j).koko = j;
                clust(j).asiakkaita = j;
                clust(j).postMd = j;
                clust(j).postYliMd = j;
                clust(j).postMax = j;
                clust(j).commentMd = j;
                clust(j).commentYliMd = j;
                clust(j).commentMax = j;
                clust(j).likePostMd = j;
                clust(j).likePostYliMd = j;
                clust(j).likePostMax = j;
                clust(j).likeCommentMd = j;
                clust(j).likeCommentYliMd = j;
                clust(j).likeCommentMax = j;
                clust(j).postPvmMd = j;
                clust(j).postPvmYliMd = j;
                clust(j).postPvmMax = j;
                clust(j).commentPvmMd = j;
                clust(j).commentPvmYliMd = j;
                clust(j).commentPvmMax = j;
                clust(j).jasen = j;
                clust(j).commentCommentMd = j;
                clust(j).commentCommentMdYli = j;
                clust(j).commentCommentMax = j;
                clust(j).postLikesMd = j;
                clust(j).postLikesMdYli = j;
                clust(j).postLikesMax = j;
                clust(j).postSharesMd = j;
                clust(j).postSharesMdYli = j;
                clust(j).postSharesMax = j;
                clust(j).likes = j;
                clust(j).postit = j;
                clust(j).postLikes = j;
                clust(j).postShares = j;
                clust(j).postComment = j;
                clust(j).postCommentComment = j;

            end
            
            for j=1:K+1
                if(j==K+1)
                    clust(j).tiedot = dataNimi;
                    clust(j).likes = obj.likes.PLlop;
                end
                
                if(j<K+1)
                clust(j).inds = find(klustData(kohta).clust(:,K) == j);
                clust(j).tiedot = dataNimi(clust(j).inds,:);
                [Lia,Locb] = ismember(clust(j).tiedot(:,end),...
                    obj.likes.PLlop(:,end));
                clust(j).likes = obj.likes.PLlop(Locb,:);
                %clust(j).likes = obj.likes.PLlop(clust(j).inds,:);

                end
                [Pia,Ipostit] = ismember(clust(j).tiedot(:,end),...
                    obj.sailData(:,end));
                clust(j).postit = obj.sailData(Ipostit,:);
                clust(j).koko = length(clust(j).tiedot);
                clust(j).asiakkaita =...
                    length(unique(clust(j).tiedot(:,kayttajaID)));
                clust(j).postMd = median(clust(j).tiedot(:,1));
                clust(j).postYliMd = round((sum(clust(j).tiedot(:,1) >...
                    (clust(j).postMd)))/(clust(j).koko)*100);
                clust(j).commentMd = median(clust(j).tiedot(:,2));
                clust(j).commentYliMd = round((sum(clust(j).tiedot(:,2) >...
                    (clust(j).commentMd)))/(clust(j).koko)*100);
                clust(j).likePostMd = median(clust(j).tiedot(:,3));
                clust(j).likePostYliMd = round((sum(clust(j).tiedot(:,3) >...
                    (clust(j).likePostMd)))/(clust(j).koko)*100);
                clust(j).likeCommentMd = median(clust(j).tiedot(:,4));
                clust(j).likeCommentYliMd = round((sum(clust(j).tiedot(:,4) >...
                    (clust(j).likeCommentMd)))/(clust(j).koko)*100);
                clust(j).postPvmMd = median(clust(j).tiedot(:,5));
                clust(j).postPvmYliMd = round((sum(clust(j).tiedot(:,5) >...
                    (clust(j).postPvmMd)))/(clust(j).koko)*100);
                clust(j).commentPvmMd = median(clust(j).tiedot(:,6));
                clust(j).commentPvmYliMd = round((sum(clust(j).tiedot(:,6) >...
                    (clust(j).commentPvmMd)))/(clust(j).koko)*100);
                clust(j).postMax = max(clust(j).tiedot(:,1));
                clust(j).commentMax = max(clust(j).tiedot(:,2));
                clust(j).likePostMax = max(clust(j).tiedot(:,3));
                clust(j).likeCommentMax = max(clust(j).tiedot(:,4));
                clust(j).postPvmMax = max(clust(j).tiedot(:,5));
                clust(j).commentPvmMax = max(clust(j).tiedot(:,6));
                clust(j).jasen = round((length(find(clust(j).tiedot(:,10)...
                    == 1))/(clust(j).koko))*100);
                clust(j).commentCommentMd = median(clust(j).tiedot(:,7));
                clust(j).commentCommentMdYli =...
                    round((sum(clust(j).tiedot(:,7) > ...
                    (clust(j).commentCommentMd)))/(clust(j).koko)*100);
                clust(j).postLikesMd = median(clust(j).tiedot(:,8));
                clust(j).postLikesMdYli = round((sum(clust(j).tiedot(:,8) >...
                    (clust(j).postLikesMd)))/(clust(j).koko)*100);
                clust(j).postSharesMd = median(clust(j).tiedot(:,9));
                clust(j).postSharesMdYli = round((sum(clust(j).tiedot(:,9)>...
                    (clust(j).postSharesMd)))/(clust(j).koko)*100);
                clust(j).commentCommentMax = max(clust(j).tiedot(:,7));
                clust(j).postLikesMax = max(clust(j).tiedot(:,8));
                clust(j).postSharesMax = max(clust(j).tiedot(:,9));
                clust(j).postLikes = sum(clust(j).tiedot(:,8));
                clust(j).postShares = sum(clust(j).tiedot(:,9));
                clust(j).postComment = sum(clust(j).postit(:,11));
                clust(j).postCommentComment = sum(clust(j).postit(:,12));
            end
            
        end
        
		
        function [variable, S] = erottelevatMuuttujat(obj, kohta, K,...
                variable, klustData)
            %% EROTTELEVATMUUTTUJAT Laskee klustereita erottelevat muuttujat
            %   Laittaa muuttujat j�rjestykseen sen mukaan, mitk� muuttujat
            %   erottelevat klusterit paremmin (prototyyppien eli klusterin
            %   keskipisteiden mukaan). J�rjest�� ensin jokaisen muuttujan
            %   prototyypit pienest� suurimpaan, jonka j�lkeen laskee
            %   klusterien prototyyppien v�liset erot ja summan niist�.
            %   J�rjest�� n�iden summien mukaan ne laskevaan j�rjestykseen.
			
            %   kohta = mist� kohtaa klusteriDataa otetaan eli mik�
            %   		klusterointi
            %   K = klusterin m��r�
            %   variable = rakenne muuttujista, joihin summatut erot
            %   klustData = data, josta otetaan prototyypit
			
            %   variable = palauttaa variable-rakenteen, jossa erojen
            %   summat
            %   S = j�rjestykseen laitetut muuttujat ja erotuksien summat.
           
            D = klustData(kohta).kluster(K).C;
            
            for j=1:length(variable)
                variable(j).erovaisuus = sum(abs(diff(sort(D(:,j)))));
            end
            
            [summat, order] = sort([variable(:).erovaisuus],'descend');
            S = variable(order);
            muuttujat = strings([1,length(S)]);
      
            for j=1:length(S)
                    muuttujat(j) = S(j).nimi;
            end
                        
            y = transpose(D(:,order));
            b = bar(y,'FaceColor','flat');
            for k = 1:size(y,2)
                b(k).CData = k;
            end

            xlim([0 (length(variable) + 1.5)]);
            axis 'auto y';
            ylabel('Prototyypin arvo');
            
            labels = cellfun(@(x) strrep(x,',','\newline'), muuttujat,...
                'UniformOutput',false);
            ax = gca;
            ax.XTickLabel = labels;
            ax.FontSize = 12;
            
            for j=1:K
                if (j == 10)
                    leg(j,:) = strcat('C', num2str(j));
                else leg(j,:) = strcat('C0', num2str(j));
                end
            end
            
            legend({leg}, 'Location','northeast');
                        
        end
        
		
        function [histot, Mbar] = histoLikes(obj, A, lkm, kLkm)
            %% HISTOLIKES Histogrammi tykk�yksien reaktioista
            %   A = datarakenne, josta tiedot otetaan
            %   lkm = Pylv�iden lkm
            %   kLkm = klustereiden lkm
			%
			%	histot = histogrammi
			%	Mbar = histogrammin pylv��t
                        
            for j=1:kLkm
                histot(j).nimi = ['K', num2str(j,'%d')];
                
                for k=lkm
                histot(j).h(1,k) = sum(A(j).likes(:,k));
                end
            end
                       
            Mbar = zeros(kLkm, length(lkm)+1);
            
            for j=1:kLkm
                Mbar(j,:) = histot(j).h; 
            end
            
            Mbar(:,4) = [];
            figure
            bar(Mbar)
            
            for j=1:kLkm
                if (j == 10)
                    labels(j,:) = strcat('C', num2str(j));
                else labels(j,:) = strcat('C0', num2str(j));
                end
            end
            
            xlim([0 (kLkm + 1.5)]);
            axis 'auto y';
            
            ax = gca;
            ax.XTickLabel = labels;
            ax.FontSize = 12;

            ylabel('Reaktioiden lukum��r�');
            xlabel('Klusterit');
            
            legend({'Vihainen', 'Haha', 'Tykk��', 'Ihastu', 'Surullinen',...
                'Vau'},'Location','northeast');

        end
        
        
        function visualisointi(obj, D)
            %% VISUALISOINTI Datan visualisointi
            %   Muodostaa annetusta datasta scatter-kuvaajan
            %   D = data
            
            dataNimi = getfield(obj, D);

            figure
            
            x = dataNimi(:,1);
            y = dataNimi(:,2);

            scatter3(x,y,dataNimi(:,3))
			
        end
        
        
        function julkaisujenTietoja(obj, A, klusterit, sar)
            %% JULKAISUJENTIETOJA Tietoja julkaisuista
            %   Pylv�sdiagrammi julkaisujen eri toimintojen lukum��rist�
            %   A = datarakenne, josta tiedot otetaan
            %   klusterit = klusteri, joita k�ytet��n
            %   sar = pylv�iden lkm per klusteri
            
            barM = zeros(length(klusterit),sar);
            
            for j=1:length(klusterit)
                barM(j,1) = A(klusterit(j)).postLikes;
                barM(j,2) = A(klusterit(j)).postShares;
                barM(j,3) = A(klusterit(j)).postComment;
                barM(j,4) = A(klusterit(j)).postCommentComment;
            end
            
            figure
            bar(barM);
            
            ylabel('Julkaisujen toimintojen lkm');
            xlabel('Klusterit');
            
            ax = gca;
            ax.FontSize = 12;
            
            legend({'Tykk�ykset', 'Jaot', 'Kommentit',...
                'Kommenttien kommentit'},'Location','northeast');
        end
        
        
       function seuratutKayttajat = kayttajatHeikostiSitoutuneetSeuraa...
                (obj, rakenne, klusterit)
            %%KAYTTAJATHEIKOSTISITOUTUNEETSEURAA Heikosti sitoutuneet
            %%seuraavat (asiakkuuden s�ilytt�miseen liittyv��)
            % K�ytt�j�t, joita heikosti sitoutuneet seuraavat
            % rakenne = datarakenne, josta tiedot otetaan
            % klusterit = klusteri, joita k�ytet��n
            
            for j=1:length(klusterit)
                seuratutKayttajat(j).klusteri...
                    = cell(rakenne(klusterit(j)).koko,3);
            end
            
            rivi = 0;
            
            for j=klusterit
                
                tiedot = rakenne(j).tiedot;
                rivi = rivi + 1;
                
                for k=1:length(tiedot)
                    
                    seuratutKayttajat(rivi).klusteri{k,1} = tiedot(k,end);
                    rowsPost = obj.tableLike.id == tiedot(k,end) &...
                        obj.tableLike.cid == 0;
                    tauluPost = obj.tableLike(rowsPost,:);
                    tauluPost = unique(tauluPost.pid);
                    rowsPost2 = ismember(obj.tablePost.pid,tauluPost);
                    Ipost = find(rowsPost2);
                    seuratutKayttajat(rivi).klusteri{k,2} =...
                        unique(obj.tablePost.id(Ipost));
                    
                    rowsComment = obj.tableLike.id == tiedot(k,end) &...
                        obj.tableLike.cid > 0;
                    tauluComment = obj.tableLike(rowsComment,:);
                    tauluComment = unique(tauluComment.cid);
                    rowsComment2 = ismember(obj.tableComment.cid,...
                        tauluComment);
                    Icomment = find(rowsComment2);
                    seuratutKayttajat(rivi).klusteri{k,3} =...
                        unique(obj.tableComment.id(Icomment));
                end
            end
        end
        
        
        function osuudetSeuratut = seurattujaKayttajat(obj, rakenne,...
                seuratutKayttajat, klusterit)
            %%SEURATTUJAKAYTTAJAT Seurattujen k�ytt�jien osuudet 
            % Laskee seurattujen k�ytt�jien osuudet 
            % (asiakkuudens�ilytt�miseen liittyv��)
            % rakenne = datarakenne, josta tiedot otetaan
            % seuratutKayttajat = seuratut kayttajat, joita k�ytet��n
            % klusterit = klusteri, joita k�ytet��n
            
            osuudetSeuratut = zeros(length(seuratutKayttajat),...
            length(klusterit));
            
            for j=1:length(seuratutKayttajat)
                
                matIdPost = unique...
                    (cell2mat(seuratutKayttajat(j).klusteri(:,2)));
                matIdComment = unique...
                    (cell2mat(seuratutKayttajat(j).klusteri(:,3)));


                for k=1:length(klusterit)
                    num = klusterit(k);
                    rowsPost = ismember(rakenne(num).tiedot(:,end),...
                        matIdPost);
                    rowsComment = ismember(rakenne(num).tiedot(:,end),...
                        matIdComment);

                    idLkmPost = length(find(rowsPost));
                    idLkmComment = length(find(rowsComment));

                    osuudetSeuratut(j,k) =...
                        (idLkmPost/rakenne(num).koko)*100;
                    osuudetSeuratut(j,k + (length(klusterit))) =...
                        (idLkmComment/rakenne(num).koko)*100;

                end
            end
        end
    end
end

