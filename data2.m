%% Asiakkuuden kehitt�misen datan k�sittely
%   Tutkitaan asiakkaan sitoutumista asiakkuuden
%   kehitt�misen vaiheessa Facebookin postauksien, kommenttien ja
%   tykk�yksien avulla.
%   Tiedonlouhinnan hy�dynt�minen asiakkaan sitoutumisen tutkimisessa -
%   Pro gradu -tutkielma
%   4.6.2019 Merja Halonen 

clear, close all;
clc;

%%Aineiston lataaminen Matlabille (aineisto otettu Kaggle.com)
case2 = AsiakkuudenKehittaminen('post.csv', 'like.csv', 'member.csv',...
                                'comment.csv');
                            
%% Muodostetaan data-matriisi, johon laitetaan kaikkien k�ytt�jien ID:t
% Sarakkeet: 1. postaukset, 2. kommentit, 3. tyk�nnyt postausta, 4. tyk�nnyt
% kommenttia, 5. postauksia p�ivitt�in, 6. kommenteja p�ivitt�in,
% 7. kommentteja kommenteihin, 8. postauksien tykk�yksi�, 9. postauksen 
% jakoja, 10. Onko j�sen, 11. ID, 12. id nimille (tuplia)
case2.kayttajatMatriisinRiveille(12);

% Tykk�yksien reaktioita k�ytt�jitt�in (angry, haha, like, likes, love, 
%   sad, wow)
case2.likesRakenne(9);


%% Lasketaan montako postausta k�ytt�jitt�in
case2.laskePost('tablePost', 1, 8, 9);


%% Lasketaan tykk�yksien lkm k�ytt�jitt�in
case2.laskeLike('tableLike', 3);


%% Lasketaan kommenttien lkm k�ytt�jitt�in
case2.laskeComment('tableComment', 2, 7);


%% Poistetaan k�ytt�j�t, joilla ei ole mit��n tietoja
case2.poistaNollarivit();


%% Vied��n monenako eri p�ivin� k�ytt�j� on postannut ja kommentoinut 
%   data-matriisiin
case2.paivatDataan([5 6]);


%% Yhdistet��n k�ytt�j�t, joilla sama nimi, mutta eri ID, koska vaihtanut 
%   profiilikuvaa
case2.yhdistaaTuplaKayttajat();
case2.yhdistaaTuplaKayttajatLikes();


%% Tarkistetaan, onko postanut, tyk�nnyt tai kommentoinut k�ytt�j� ryhm�n j�sen
case2.ryhmanJasen('tableMember', 'lopData');


%% Histogrammit muuttujista
x_Labels1 = {'Julkaisujen lkm', 'Kommenttien lkm',...
    'Tykk�yksien lkm julkaisuissa', 'Tykk�yksien lkm kommenteissa'};  
case2.histogrammiMuuttujat('lopData', x_Labels1);

x_Labels2 = { 'Postauksia p�ivitt�in', 'Kommentteja p�ivitt�in'};
case2.histogrammiMuuttujat('lopData', x_Labels2);

% Histogrammit eri sarakkeista
figure;
histogram(case2.lopData(:,6));

figure;
histogram(case2.lopData(:,7));

figure;
histogram(case2.lopData(:,3),200);

figure;
histogram(case2.lopData(:,4),200);

figure;
histogram(case2.lopData(:,2),200);

figure;
scatter(case2.lopData(:,1), case2.lopData(:,2));


%% Min-max-skaalaus lopDatasta skaalattudataksi
case2.minMaxSkaalaus('lopData', 'skaalattuData', 6);


%% Logaritminen muunnos lopDatasta logDataksi
case2.logaritmiMuutos('lopData', 'logData', 6);


%% Min-max-skaalaus logDatasta logSkaalattuDataksi
case2.minMaxSkaalaus('logData', 'logSkaalattuData', 6);

%% Otetaan k�ytt�j�t, joilla postauksia alle 15 tai enemm�n
case2.poistaaTietytKayttajat('lopData', 'yli', 1, 15);
case2.poistaaTietytKayttajat('dataPoisRiveja', 'yli', 2, 60);
case2.poistaaTietytKayttajat('dataPoisRiveja', 'yli', 3, 70);
case2.poistaaTietytKayttajat('dataPoisRiveja', 'yli', 4, 90);


%% Min-max-skaalaus skaalatulle datalle, poistettu tarpeettomia k�ytt�ji�
case2.minMaxSkaalaus('dataPoisRiveja', 'skaalattudataPoisRiveja', 6);


%% Logaritminen muunnos dataPoisRiveja:sta logDataPoisRiveja:ksi
case2.logaritmiMuutos('dataPoisRiveja', 'logDataPoisRiveja', 6);


%% Min-max-skaalaus eri logDataPoisRiveja:st� logSkaalattuDataPois:ksi 
case2.minMaxSkaalaus('logDataPoisRiveja', 'logSkaalattuDataPois', 6);


%% Histogrammit muuttujista
figure;
case2.histogrammiMuuttujat('dataPoisRiveja', x_Labels2);

figure;
histogram(case2.dataPoisRiveja(:,1));

figure;
histogram(case2.dataPoisRiveja(:,5));

figure;
histogram(case2.dataPoisRiveja(:,6));


%% Asiakkaan s�ilytt�miseen liittyv�t tiedot
case2.laskePostuksienComment(2, 'tablePost', 'tableComment');


%% Tyhjennet��n datoja viem�st� muistia
case2.tyhjennaData('data');
case2.tyhjennaData('paivat');
case2.tyhjennaData('lopId');
case2.tyhjennaData('tablePost');
case2.tyhjennaData('tableComment');
case2.tyhjennaData('tableLike');
case2.tyhjennaData('tableMember');


%% Klusterointi

% Klustereita (K) 1-20 ja klusteroinnin toistojen m��r� (r)
[N,n] = size(case2.lopData);
K = 20;
r = 100;

% Rakenne, joka sis�lt�� klusteroinnin tulokset jokaiselle datamuunnokselle
 nimet(1).nimi = 'KaikkiMuuttujat';
 nimet(2).nimi = 'EiPaivat';
 nimet(3).nimi = 'SkaalausKaikkiMuuttujat';
 nimet(4).nimi = 'SkaalausEiPaivat';
 nimet(5).nimi = 'LogKaikkiMuuttujat';
 nimet(6).nimi = 'LogEiPaivat';
 nimet(7).nimi = 'LogSkaalattuKaikkiMuuttujat';
 nimet(8).nimi = 'LogSkaalattuEiPaivat';
 nimet(9).nimi = 'SkaalattuDataPoisRivejaKaikkiMuuttujat';
 nimet(10).nimi = 'SkaalattuDataPoisRivejaEiPaivat';
 nimet(11).nimi = 'LogPoisSkaalattuKaikkiMuuttujat';
 nimet(12).nimi = 'LogPoisSkaalattuEiPaivat';

 % Sarakkeet, joista tiedot otetaan klusterointiin
 valit(1).eka = 1;
 valit(1).vika = 6;
 valit(2).eka = 1;
 valit(2).vika = 4;
 valit(3).eka = 1;
 valit(3).vika = 6;
 valit(4).eka = 1;
 valit(4).vika = 4; 
 valit(5).eka = 1;
 valit(5).vika = 6;
 valit(6).eka = 1;
 valit(6).vika = 4;
 valit(7).eka = 1;
 valit(7).vika = 6;
 valit(8).eka = 1;
 valit(8).vika = 4;
  valit(9).eka = 1;
 valit(9).vika = 6;
 valit(10).eka = 1;
 valit(10).vika = 4;
  valit(11).eka = 1;
 valit(11).vika = 6;
 valit(12).eka = 1;
 valit(12).vika = 4;


 % Muodostetaan data-rakenne, johon laitetaan klusteroinnin tulokset
 caseK = Klusterointi(nimet, 'klustData');

 % Klusteroitaan valitut datat
 caseK.klusterointiData(case2.lopData, case2.skaalattuData,...
     case2.logData, case2.logSkaalattuData,case2.skaalattudataPoisRiveja,...
     case2.logSkaalattuDataPois, K, r, nimet, valit, 'klustData');
 
 
%% Klusterien validointi
 
% Muodostetaan indeksit-rakenne, johon validointi-indeksit tallennetaan
 caseV = KlusterienValidointi(nimet);
 
% Validoitaan valitut klusterit
 caseV.validationIndeksit(case2.lopData, case2.skaalattuData,...
     case2.logData, case2.logSkaalattuData,case2.skaalattudataPoisRiveja,...
     case2.logSkaalattuDataPois, K, nimet, valit, caseK.klustData);
	 
% Tallennetaan lasketut ja k�sitellyt tiedot tiedostoon toistettavuuden ja 
% ja laskennan nopeuttamisen vuoksi (sis. klusteroinnin
% ja klusteroinnin, joiden laskenta vie aikaa).

%  save('case2_validointiIndeksit.mat','case2', 'caseK', 'caseV');

% Ladataan tallennettu tiedosto
load('case2_validointiIndeksit.mat');

 
%% Knee-point-kuvaajat eri data-muunnoksista 
case2.kneePointKuvaaja(caseV.indeksit, K, nimet, 2);
 
nimet(1).nimi = 'LogSkaalattuKaikkiMuuttujat';
case2.valitutKuvaajat(caseV.indeksit, K, nimet, 2, [8 23 20], 7, 3)

nimet(1).nimi = 'LogSkaalattuEiPaivat';
case2.valitutKuvaajat(caseV.indeksit, K, nimet, 2, [8 23 20], 8, 3)

nimet(1).nimi = 'LogPoisSkaalattuKaikkiMuuttujat';
case2.valitutKuvaajat(caseV.indeksit, K, nimet, 2, [8 23 20], 11, 3)


%% Klusteroinnin tarkastelu

 kohta = 7; %data-rakenteen kohta, jota tarkastellaan
 kLkm = 9; 
 [A, dataMdPost, dataMdComment, dataMdLikePost, dataMdLikeComment] =...
     case2.klusterinTarkastelu('lopData', kLkm, kohta, 11, caseK.klustData);
 
 kohta = 8; %data-rakenteen kohta, jota tarkastellaan
 kLkm = 10;
 [B, BdataMdPost, BdataMdComment, BdataMdLikePost, BdataMdLikeComment] =...
     case2.klusterinTarkastelu('lopData', kLkm, kohta, 11, caseK.klustData);
 
% Rakenne muuttujiin, jotka erottelevat muuttujat
			
variable(1).nimi = 'Julkaisut';
variable(2).nimi = 'Kommentit'; 
variable(3).nimi = 'Julkaisujen,tykk�ykset'; 
variable(4).nimi = 'Kommenttien,tykk�ykset';
variable(5).nimi = 'Julkaisujen,pvm lkm'; 
variable(6).nimi = 'Kommenttien,pvm lkm'; 


%Lasketaan klustereita erottelevat muuttujat
%   Laittaa muuttujat j�rjestykseen sen mukaan, mitk� muuttujat
%   erottelevat klusterit paremmin (prototyyppien eli klusterin
%   keskipisteiden mukaan). 
[variable, S] = case2.erottelevatMuuttujat(kohta, kLkm, variable,...
    caseK.klustData);
[variable, S] = case2.erottelevatMuuttujat(kohta, kLkm, variable,...
    caseK.klustData);

% Histogrammit tykk�yksien reaktioista
[histot, Mbar] = case2.histoLikes(A, [1 2 3 5 6 7], kLkm);
[histot, Mbar] = case2.histoLikes(B, [1 2 3 5 6 7], kLkm);

% Pylv�sdiagrammi julkaisujen eri toimintojen lukum��rist�
case2.julkaisujenTietoja(A, [1,7,5], 4);
case2.julkaisujenTietoja(B, [5,9,10,6,7,8], 4);

% Datan visualisointia
case2.visualisointi('lopData');


%% Uusi klusterointi, kun poistettu enemm�n rivej� datasta
% Klustereita (K) 1-20 ja klusteroinnin toistojen m��r� (r)

[N,n] = size(case2.dataPoisRiveja);
K = 20;
r = 100;

% Rakenne, joka sis�lt�� klusteroinnin tulokset jokaiselle datamuunnokselle
nimet(1).nimi = 'SkaalattuDataPoisRivejaKaikkiMuuttujat';
nimet(2).nimi = 'SkaalattuDataPoisRivejaEiPaivat';
nimet(3).nimi = 'LogPoisSkaalattuKaikkiMuuttujat';
nimet(4).nimi = 'LogPoisSkaalattuEiPaivat';

% Sarakkeet, joista tiedot otetaan klusterointiin
valit(1).eka = 1;
valit(1).vika = 6;
valit(2).eka = 1;
valit(2).vika = 4;
valit(3).eka = 1;
valit(3).vika = 6;
valit(4).eka = 1;
valit(4).vika = 4;

% Muodostetaan data-rakenne, johon laitetaan klusteroinnin tulokset
caseK = Klusterointi(nimet, 'klustData');

% Klusteroidaan valitut datat
caseK.klusterointiData(case2.skaalattudataPoisRiveja,...
    case2.logSkaalattuDataPois, case2.logData,...
    case2.logSkaalattuData,case2.skaalattudataPoisRiveja,...
    case2.logSkaalattuDataPois,K, r, nimet, valit, 'klustData');

	
%% Klusterien validointi
 
% Muodostetaan indeksit-rakenne, johon validointi-indeksit tallennetaan
caseV = KlusterienValidointi(nimet);

% Validoitaan valitut klusterit
caseV.validationIndeksit(case2.skaalattudataPoisRiveja,...
    case2.logSkaalattuDataPois, case2.logData,...
    case2.logSkaalattuData, case2.skaalattudataPoisRiveja,...
    case2.logSkaalattuDataPois,K, nimet, valit, caseK.klustData);

% Tallennetaan lasketut ja k�sitellyt tiedot tiedostoon toistettavuuden ja 
% ja laskennan nopeuttamisen vuoksi (sis. klusteroinnin
% ja klusteroinnin, joiden laskenta vie aikaa).

% save('case2_validointiIndeksitYksittainen.mat','case2', 'caseK', 'caseV');

% Ladataan tallennettu tiedosto
load('case2_validointiIndeksitYksittainen.mat');

%% Knee-point-kuvaajat eri data-muunnoksista
case2.kneePointKuvaaja(caseV.indeksit, K, nimet, 2);

nimet(1).nimi = 'LogPoisSkaalattuKaikkiMuuttujat';
case2.valitutKuvaajat(caseV.indeksit, K, nimet, 2, [8 23 20], 3, 3)


%% Klusteroinnin tarkastelu

 kohta = 3; %data-rakenteen kohta, jota tarkastellaan
 kLkm = 10;
 [C, CdataMdPost, CdataMdComment, CdataMdLikePost,...
     CdataMdLikeComment] = case2.klusterinTarkastelu('dataPoisRiveja',...
     kLkm, kohta, 11, caseK.klustData);

% Rakenne muuttujiin, jotka erottelevat muuttujat	 
variable(1).nimi = 'Julkaisut';
variable(2).nimi = 'Kommentit'; 
variable(3).nimi = 'Julkaisujen,tykk�ykset'; 
variable(4).nimi = 'Kommenttien,tykk�ykset';
variable(5).nimi = 'Julkaisujen,pvm lkm'; 
variable(6).nimi = 'Kommenttien,pvm lkm'; 

%Lasketaan klustereita erottelevat muuttujat
%   Laittaa muuttujat j�rjestykseen sen mukaan, mitk� muuttujat
%   erottelevat klusterit paremmin (prototyyppien eli klusterin
%   keskipisteiden mukaan).
[variable, S] = case2.erottelevatMuuttujat(kohta, kLkm, variable,...
    caseK.klustData);

% Pylv�sdiagrammi julkaisujen eri toimintojen lukum��rist�
[histot, Mbar] = case2.histoLikes(C, [1 2 3 5 6 7], kLkm);

 
%% Klusterointi K-Medians
 
% Muodostetaan data-rakenne, johon laitetaan klusteroinnin tulokset 
 caseKM = KlusterointiMedian(nimet, 'klustData');

% Klusteroidaan valitut datat 
 caseKM.klusterointiData(case2.lopData, case2.skaalattuData,...
     case2.logData,case2.logSkaalattuData,case2.skaalattudataPoisRiveja,...
     case2.logSkaalattuDataPois, K, r, nimet, valit, 'klustData');
 
%% Klusterien validointi

% Muodostetaan indeksit-rakenne, johon validointi-indeksit tallennetaan
 caseVM = KlusterienValidointiMedian(nimet);
 
% Validoitaan valitut klusterit
 caseVM.validationIndeksit(case2.lopData, case2.skaalattuData,...
     case2.logData, case2.logSkaalattuData, case2.skaalattudataPoisRiveja,...
     case2.logSkaalattuDataPois, K, nimet, valit, caseKM.klustData);
	 
% Tallennetaan lasketut ja k�sitellyt tiedot tiedostoon toistettavuuden ja 
% ja laskennan nopeuttamisen vuoksi (sis. klusteroinnin
% ja klusteroinnin, joiden laskenta vie aikaa).

%   save('case2_validointiIndeksitMedian.mat','case2', 'caseKM', 'caseVM');

% Ladataan tallennettu data
%load('case2_validointiIndeksitMedian.mat');

% KneePoint kuvaaja
case2.kneePointKuvaaja(caseVM.indeksit, K, nimet, 2);


%% Klusteroinnin tarkastelua

 kohta = 7; %data-rakenteen kohta, jota tarkastellaan
 kLkm = 9; 
 
 [M, MdataMdPost, MdataMdComment, MdataMdLikePost, MdataMdLikeComment] =...
     case2.klusterinTarkastelu('lopData', kLkm, kohta, 11,...
     caseKM.klustData);

	 
%% Asiakkuuden s�ilytt�miseen k�ytt�j�t, joita heikosti sitoutuneet seuraa
 
% Selvitet��n k�ytt�j�t, joita heikosti sitoutuneet seuraavat
seuratutKayttajat =  case2.kayttajatHeikostiSitoutuneetSeuraa(A, [2,6,8]);
seuratutKayttajat =  case2.kayttajatHeikostiSitoutuneetSeuraa(B, [4,3]);

% Lasketaan seurattujen k�ytt�jien osuudet
osuudetSeuratut = case2.seurattujaKayttajat(A, seuratutKayttajat, [1,7,5]);
osuudetSeuratut = case2.seurattujaKayttajat(A, seuratutKayttajat, [3,9,4]);
osuudetSeuratut = case2.seurattujaKayttajat(B, seuratutKayttajat, [2,1]);
osuudetSeuratut2 = case2.seurattujaKayttajat(B, seuratutKayttajat,...
    [5,9,10,6,7,8]);
