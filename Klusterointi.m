classdef Klusterointi < handle
    %KLUSTEROINTI K-means klusteroinnin tekev� luokka.
    %   Klusteroi sille annetun datan k�ett�en K-means menetelm��.
    
    properties
        klustData;
        klustDataHier;
    end
    
	
    methods
        function obj = Klusterointi(nimet, data)
            %KLUSTEROINTI Konstruktori K-means klusterointiin
            %   Muodostaa klustData-rakenteen, johon sijoittaa
            %   klusteroinnin tulokset.
            k = length(nimet);
            
            for j=1:k
                obj = setfield(obj, data, {j}, 'nimi', nimet(j).nimi);
                obj = setfield(obj, data, {j}, 'kluster', {j});
                obj = setfield(obj, data, {j}, 'clust', {j});
            end
        end
        
		
        function klusterointiData(obj, D, SD, LD, LSD, SP, LSP,...
				K, r, nimet, valit, data)
            %% KLUSTEROINTIDATA P��tt�� datan, jolla klusteroidaan. 
            %   Kutsuu klusterit-funktiota.
            %   D, SD, LD, LSD, LSDM, SP, LSP, LSPM = data-muunnokset, 
            %   joille klusterointi tehd��n.
            %   K = klusterim��r�, johon asti klusteroidaan
            %   r = klusteroinnin toistojen m��r�
            %   nimet = klusterointi, joka tehd��n
            %   valit = muuttujat, joista klusterointi tehd��n
            %   data = attribuuttina oleva data, johon klusterointitulos
            
            k = length(nimet);

            for j=1:k
                
                if (j >= 3 & j < 5)
                    D = SD;
                elseif (j >= 5 & j < 7)
                    D = LD;
                elseif (j >= 7 & j < 9)
                    D = LSD;
                elseif (j >= 9 & j < 11)
                    D = SP;
                elseif (j >= 11)
                    D = LSP;
                end
                
                if (length(valit(j).eka) > 1)
                    sar = valit(j).eka;
                else
                    sar = valit(j).eka:valit(j).vika;
                end
                
                [kluster, idx] = obj.klusterit(D, K, r, sar);
                
                obj = setfield(obj, data, {j}, 'kluster', kluster);
                obj = setfield(obj, data, {j}, 'clust', idx);

            end
        end
        
		
        function [kluster, idx] = klusterit(obj, D, K, r, sar)
            %% KLUSTERIT Muodostaa rakenteen, mihin klusterointi otetaan.
            %   Muodostaa rakenteen, jossa on klusteroinnin tulokset eli
            %   mihin klusteriin mik�kin havainto menee. Kutsuu
            %   klusterointi-funktiota
            % protolabels = kertoo, mihin klusteriin havainto kuuluu.
            % C = prototyyppien eli klustereiden keskiarvojen koordinaatit
            % SumD = Within-cluster sums of point-to-centroid distances. 
            % Klustereiden pisteiden et�isyyksien summa klusterin keskiarvoon
            % Dkm = Distances from each point to every centroid. 
            %       Pisteen et�isyys klustereiden keskiarvoon(kohtaan)
            %
			%	D = data
            %   K = klusterim��r�, johon asti klusteroidaan
            %   r = klusteroinnin toistojen m��r� 
			%	sar = datan sarakkeet, joista klusterointi tehd��n
			%
            %   [kluster] = Kaikki klusteroinnin osat
            %   [idx] = klusterin protolabelit eli mihin klusteriin
            %   		havainto kuuluu.
            
            for j=1:K
                kluster(j).protoLabels = j;
                kluster(j).C = j;
                kluster(j).SumD = j;
                kluster(j).Dkm = j;
            end
            
            for j=1:K
                
                kluster(j) = obj.klusterointi(D, j, r, sar);
                idx(:,j) = kluster(j).protoLabels;
            end
        end
        
		
        function A = klusterointi(obj, D, K, r, sar)
            %% KLUSTEROINTI K-means klusterointi datalle
            %   Klusteroi datan halutulla klusterim��r�ll� ja halutulla 
            %   toistolla
            %   D = data, jolle klusterointi tehd��n
            %   K = klustereiden m��r�
            %   r = klusteroinnin toistojen m��r�
			%	sar = datan sarakkeet, joista klusterointi tehd��n
			
            [ProtoLabels,C,SumD,Dkm] = kmeans(D(:,sar),K,'replicates',r,...
                'MaxIter',10000);
            
            A.protoLabels = ProtoLabels;
            A.C = C;
            A.SumD = SumD;
            A.Dkm = Dkm;
        
		end
    end
end

