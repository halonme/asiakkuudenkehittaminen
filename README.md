AsiakkuudenKehittaminen on Merja Halosen pro gradu -tutkielman yhden osa-alueen Matlab:llä muodostettu lähdekoodi. 
Pro gradu -tutkielma (2019) käsittelee Knowledge Discovery in Databases (KDD) -
prosessin soveltamista asiakkaan sitoutumisen tutkimiseen asiakkuuden elinkaaren eri vaiheissa, 
joista yksi on asiakkuuden kehittäminen. Asiakkuuden kehittämisen tutkimiseen on käytetty klusterointia.
Linkki pro graduun http://urn.fi/URN:NBN:fi:jyu-201906203343


**Data2.m** -tiedosto on ajettava päätiedosto, joka kutsuu muita luokkia (tiedostoja) seuraavasti:

+ **AsiakkuudenKehittaminen.m** -tiedosto luokka sisältää datan esikäsittelyn, muunnokset, tuloksien tulkinnan ja visualisoinnin.
+ **Klusterointi.m** -tiedoston luokka klusteroi sille annetun datan käyttäen K-means menetelmää.
+ **KlusterienValidointi.m** -tiedoston luokka validoi erilaisilla validointi-indekseillä K-means-menetelmällä muodostetut klusterit.
+ **KlusterointiMedian.m** -tiedoston luokka klusteroi sille annetun datan käyttäen K-medians menetelmää.
+ **KlusterienValidointiMedian.m** -tiedoston luokka validoi erilaisilla validointi-indekseillä K-medians-menetelmällä muodostetut klusterit.

AsiakkuudenKehittamisessa datana on käytetty Kagglesivustolta saatua Cheltenhamin julkisen yhteisön Facebookin julkaisuja vuoden 2008 heinäkuun ja vuoden 2016 kesäkuun väliseltä ajalta
https://www.kaggle.com/mchirico/cheltenham-s-facebook-group/version/1/home.
