classdef KlusterienValidointiMedian < handle
    %KLUSTERIENVALIDOINTI Luokka, joka validoi klusterit
    %   Muodostaa validointi-indeksit jokaiselle klusteroidulle datalle.
    
    properties
        indeksit;
    end
    
    methods
        function obj = KlusterienValidointiMedian(nimet)
            %KLUSTERIENVALIDOINTI Konstruktori klusterien (median) 
            %                       validointiin
            %   Konstruktori, muodostaa indeksit-rakentee, johon
            %   validointi-indeksi tallennetaan.
            %   nimet = tehdyt eri klusteroinnit
            k = length(nimet);
            
            for j=1:k 
                obj.indeksit(j).nimi = nimet(j).nimi;
                obj.indeksit(j).WB = j;
                obj.indeksit(j).WBvalue = j;
                obj.indeksit(j).WBk = j;
                obj.indeksit(j).WG = j;
                obj.indeksit(j).WGvalue = j;
                obj.indeksit(j).WGk = j;
                obj.indeksit(j).KCE = j;
                obj.indeksit(j).KCEvalue = j;
                obj.indeksit(j).KCEk = j;
                obj.indeksit(j).PBM = j;
                obj.indeksit(j).PBMvalue = j;
                obj.indeksit(j).PBMk = j;
                obj.indeksit(j).RT = j;
                obj.indeksit(j).RTvalue = j;
                obj.indeksit(j).RTk = j;
                obj.indeksit(j).SILH = j;
                obj.indeksit(j).SILHvalue = j;
                obj.indeksit(j).SILHk = j;
            end
            
            
        end
        
        function validationIndeksit(obj, D, SD, LD, LSD, SP, LSP,...
                K, nimet, valit, klustData)
            %% VALIDOINTIINDEKSIT Antaa data, josta indeksit lasketaan.
            %   Kutsuu eri validointi-funkioita
            %   D, SD, LD, LSD, LSDM, SP, LSP, LSPM = data, joille
            %   klusterointi tehd��n
            %   K = klusterim��r�, johon asti klusteroidaan
            %   r = klusteroinnin toistojen m��r�
            %   nimet = klusterointi, joka tehd��n
            %   valit = muuttujat, joista klusterointi tehd��n
            %   klustData = klusterointidata
            
            k = length(nimet);
            
            for j=1:k
                
                if (j >= 3 & j < 5)
                    D = SD;
                elseif (j >= 5 & j < 7)
                    D = LD;
                elseif (j >= 7 & j < 9)
                    D = LSD;
                elseif (j >= 9 & j < 11)
                    D = SP;
                elseif (j >= 11)
                    D = LSP;
                end
                
                
                if (length(valit(j).eka) > 1)
                    sar = valit(j).eka;
                else
                    sar = valit(j).eka:valit(j).vika;
                end
                
                %WB
                [obj.indeksit(j).WB, obj.indeksit(j).WBvalue,...
                    obj.indeksit(j).WBk] =...
                    obj.evalWB(klustData(j).kluster, K, D,...
                    sar);
                 
                %WG
                [obj.indeksit(j).WG, obj.indeksit(j).WGvalue,...
                    obj.indeksit(j).WGk] =...
                    obj.evalWG(klustData(j).kluster, K, D); 
                
                %KCE
                [obj.indeksit(j).KCE, obj.indeksit(j).KCEvalue,...
                    obj.indeksit(j).KCEk] =...
                    obj.evalKCE(klustData(j).kluster, K);
                
                %PBM
                [obj.indeksit(j).PBM, obj.indeksit(j).PBMvalue,...
                    obj.indeksit(j).PBMk] =...
                    obj.evalPBM(klustData(j).kluster, K); 
                
                %RT
                [obj.indeksit(j).RT, obj.indeksit(j).RTvalue,...
                    obj.indeksit(j).RTk] =...
                    obj.evalRT(klustData(j).kluster, K,...
                    D); %evalRT(obj, kluster, K, D)
                
                %SILH
                [obj.indeksit(j).SILH, obj.indeksit(j).SILHvalue,...
                    obj.indeksit(j).SILHk] =...
                    obj.evalSILH(D, klustData(j).clust, K,...
                    sar); %evalSILH(obj, D,  clust, K, sar)
            end
        end

        
        function [WB, value, k] = evalWB(obj, kluster, K, D, sar)
            %% EVALWB WB-indeksi, min arvo paras.
            %   Min-arvo paras arvo
            %   kluster = klusterien kaikki tiedot, josta indeksit lasketaan
            %   K = klusterin enimm�ism��r�
            %   D = alkuper�inen data
            
            WB = zeros(K,1);
            indx = 0;
                        
            for j=2:K
                %intra
                intra = j * (sum(min(kluster(j).Dkm,[],2)));
                
                %inter
                normiPisteMedian = sum(bsxfun(@minus,kluster(j).C,...
                    median(D(:,sar))).^1,2);
                
                A = (kluster(j).protoLabels == 1:j);
                nk = sum(A);
                
                inter = sum(nk'.*normiPisteMedian);
                                
                %intra/inter
                
                indx = intra/inter;
                WB(j) = indx;
                
            end
            
            [value, k] = min(WB(2:end));
            k = k + 1;
            
        end
        
        function [CH, value, k] = evalCH(obj, D, clust, K, sar)
            %% EVALCH Calinski-Harabasz validointi-indeksin muodostaminen
            %   Optimaalisin klustereiden m��r� on, kun Calinski-Harabasz
            %   indeksi arvo on korkein
            %   Otetaan k��nteisluku, jotta saadaan muutettua min-arvoksi 
            %   vertailun vuoksi.
            %   D = alkuper�inen data
            %   clust = protolabelsit eli tiedot, mihin klusteriin kukakin
            %   k�ytt�j� menee
                        
            CH = zeros(K,1);
            CHeiKaanteinen = zeros(K,1);
            
            CHindeksi = evalclusters(D(:,sar),clust(:,2:K),...
                'CalinskiHarabasz','Distance','cityblock');
            CHeiKaanteinen(2:K) = transpose(CHindeksi.CriterionValues);
            %otetaan k��nteinen luku, jotta saadaan pienin arvo parempi
            CH(2:K) = 1./CHeiKaanteinen(2:K);
            
            [value, k] = min(CH(2:end));
            k = k + 1;
        end
        
        
        function [DB, value, k] = evalDB(obj, D, clust, K, sar)
            %% EVALDB Davies-Bouldin validointi-indeksi muodostaminen
            %   Optimaalisin klusterointi on, kun Davies-Bouldin indeksi
            %   arvo on pienin.
            %   D = alkuper�inen data
            %   clust = protolabelsit eli tiedot, mihin klusteriin kukakin
            %   k�ytt�j� menee   
            
            DB = zeros(K,1);
            
            DBindeksi = evalclusters(D(:,sar),clust(:,2:K),...
                'DaviesBouldin','Distance','cityblock');
            
            DB(2:K) = transpose(DBindeksi.CriterionValues);
            k = DBindeksi.OptimalK;
            value = min(DB(2:end));

        end
        
        
        function [SILH, value, k] = evalSILH(obj, D,  clust, K, sar)
            %% EVALSILH silhoutte validointi-indeksin laskeminen
            %   Korkeimmasta arvosta otetaan k��nteisluku, jotta
            %   saadaan muutettua min-arvoksi vertailun vuoksi
            %   D = alkuper�inen data
            %   clust = protolabelsit eli tiedot, mihin klusteriin kukakin
            %   k�ytt�j� menee
            SILH = zeros(K,1);
            
            SILHindeksi = evalclusters(D(:,sar),clust(:,2:K),...
                'silhouette','Distance','cityblock');
            
            SILHeiKaanteinen(2:K) = transpose(SILHindeksi.CriterionValues);
            SILH(2:K) = 1./SILHeiKaanteinen(2:K);
%             k = SILHindeksi.OptimalK;
            [value, k] = min(SILH(2:end));
            k = k + 1;
        end
        
        
        function [WG, value, k] = evalWG(obj, kluster, K, D)
            %% EVALWG Wemmert-Gan�arski validointi-indeksi muodostaminen
            %   max arvo oikeata m��r� klustereita. Otetaan k��nteisarvi,
            %   jotta min-arvo vertailun vuoksi.
            %   kluster = klusterien kaikki tiedot, josta indeksit lasketaan
            %   K = klusterin enimm�ism��r�
            %   D = alkuper�inen data
            
            [N,n] = size(D);
            
            WG = zeros(K,1);
            WGeiKaanteinen = zeros(K,1);
            
            
            for j=2:K
                %intra
                intra = min(kluster(j).Dkm,[],2);
                
                %inter
                sortattu = sort(kluster(j).Dkm,2);
                inter = sortattu(:,2);
                
                %intra/inter
                sumJako = 0;
                
                for k=1:j
                    
                    A = (kluster(j).protoLabels == k);
                    nk(k) = sum(A);
                    sumJako = sumJako + max([0 (nk(k) -...
                        sum(A.*(intra./inter)))]);
                end
                %index
                WGeiKaanteinen(j) = sumJako/N;
            end
            
             %otetaan k��nteinen luku, jotta saadaan min arvo
            WG(2:end) = 1./WGeiKaanteinen(2:end); 
            
            [value, k] = min(WG(2:end));
            k = k + 1;
            
        end
        
        
        function [PBM, value, k] = evalPBM(obj, kluster, K)
            %% EVALPBM Pakhira, Bandyopadhyay, and Maulik validointi-indeksi
            %   min arvo paras
            %   kluster = klusterien kaikki tiedot, josta indeksit lasketaan
            %   K = klusterin enimm�ism��r�
            
            PBM = zeros(K,1);
            indx = 0;
            J1 = sum(kluster(1).Dkm);
            
            for j=2:K
                %intra
                intra = j * (sum(min(kluster(j).Dkm,[],2)));
                
                
                %inter
                inter = (max(pdist(kluster(j).C))^1) * J1;
                
                %intra/inter
                
                indx = (intra/inter)^2;
                PBM(j) = indx;
                
            end
            
            [value, k] = min(PBM(2:end));
            k = k + 1;
            
            
        end
        
        
        function [KCE, value, k] = evalKCE(obj, kluster, K)
            %% EVALKCE KCE validointi-indeksin muodostaminen 
            %   Pienin arvo on paras arvo
            %   kluster = klusterien kaikki tiedot, josta indeksit lasketaan
            %   K = klusterin enimm�ism��r�
            
            KCE = zeros(K,1);
            
            for j=2:K
                %intra
                KCE(j) = j * (sum(min(kluster(j).Dkm,[],2)));
            end
            
            [value, k] = min(KCE(2:end));
            k = k + 1;
            
        end
        
        function [RT, value, k] = evalRT(obj, kluster, K, D)
            %% EVALRT Ray-turi validointi-indeksin laskeminen
            %   Pienin on paras arvo
            %   kluster = klusterien kaikki tiedot, josta indeksit lasketaan
            %   K = klusterin enimm�ism��r�
            %   D = alkuper�inen data
                        
            [N,n] = size(D);
            
            RT = zeros(K,1);
            
            for k=2:K
                RT(k) = sum(min(kluster(k).Dkm,[],2))/...
                    (N*min(pdist(kluster(k).C))^1); %Ray-Turi
            end
            
            [value, k] = min(RT(2:end));
            k = k + 1;
            
        end
    end
end

